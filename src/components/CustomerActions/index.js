import React from 'react';
import PropTypes from 'prop-types';

const CustomerActions = ({ childrens }) => {
    return (
        <div>
            <div className="customer-actions">
                <div classNAme="childrens">
                    { childrens }
                </div>
            </div>
        </div>
    );
};

CustomerActions.propTypes = {
    childrens: PropTypes.node.isRequired,// El tipo nodo, es cualquier elemento renderizable de react
};

export default CustomerActions;