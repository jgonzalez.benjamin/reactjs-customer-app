import React from 'react';
import PropTypes from 'prop-types';

const CustomerData = props => {
    const customer  = {
        "id": "32165LA1M@33",
        "name": "Benja",
        "lastName": "Gonzalez",
        "age": 25,
        "dni": 30786510,
        "email": "b.gonzalez@sieteideas.com.ar",
        "position": "realtive"
    }
    return (
        <div>
            <div className="customer-data" >
                <h2> ` Datos del cliente ${customer.id} ` </h2>
                <div className="customer-name">
                    <strong>Nombre/s:</strong> <i>` ${customer.lastName}, ${customer.name} `</i>
                </div>
                <div className="customer-age" >
                    <strong>Edad:</strong> <i>` ${customer.age} `</i>
                </div>
                <div className="customer-dni" >
                    <strong>Dni:</strong> <i>` ${customer.dni} `</i>
                </div>
                <div className="customer-email">
                    <strong>Email:</strong> <i>` ${customer.email} `</i>
                </div>
                <div className="customer-position">
                    <strong>Position:</strong> <i>` ${customer.position} `</i>
                </div>
            </div>
        </div>
    );
};

CustomerData.propTypes = {
    props: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        age: PropTypes.number.isRequired,
        dni: PropTypes.number.isRequired,
        email: PropTypes.string.isRequired,
        position: PropTypes.string.isRequired,
    }),
};

export default CustomerData;