import React from 'react';
import PropTypes from 'prop-types';
import AppHeader from '../AppHeader';

const AppFrame = ({ header, body, footer }) => {

    /* Este componente nos sirve de container, asi no replicamos el titulo y el footer, en las distintas pantallas :D */
    return (
        <div>
            <div className="app-frame">

                <AppHeader title={header} />
                {/* App Body  */}
                <div className="body">
                    {body}
                </div>
                {/* App Footer  */}
                <div classNAme="footer">
                    App simple, ejemplo entendimiento reactJs - udemy courses
                </div>

            </div>
        </div>
    );
};

AppFrame.propTypes = {
    header: PropTypes.string.isRequired,
    body: PropTypes.element.isRequired,
};

export default AppFrame;