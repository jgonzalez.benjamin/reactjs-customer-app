import React from 'react';
import { Link, BrowserRouter as Router } from 'react-router-dom';
import { AppFrame } from './components';
import './App.css';

function App() {
	return (
		<Router>
			<div className="App">
				<Link to="/customers" component="AppFrame" > Customers </Link>
			</div>
		</Router>
	);
}

export default App;
